const initState = {
    auth: {
        isAuthenticated: false,
        user: {}
    }
}

export default initState;