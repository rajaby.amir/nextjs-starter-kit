import * as actionTypes from '../lib/actionTypes';
import initState from '../lib/initialStore';

const authReducer = (state = initState, action) => {
    switch (action.type) {
        case actionTypes.SET_ISAUTHENTICATED:
            return Object.assign({}, state, {
                isAuthenticated: action.auth
            });
        case actionTypes.SET_AUTH_USER:
            return Object.assign({}, state, {
                user: action.user
            });

        default: return state
    }
};

export default authReducer;