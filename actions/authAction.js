import * as actionTypes from '../lib/actionTypes';

export const setAuth = (auth) => dispatch => {
    return dispatch({ type: actionTypes.SET_ISAUTHENTICATED, auth });
}

export const setAuthUser = (user) => dispatch => {
    return dispatch({ type: actionTypes.SET_AUTH_USER, user });
}